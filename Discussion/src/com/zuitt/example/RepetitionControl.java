package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {
    public static void main(String[]args){
        // [SECTION] Loops
        //While Loop
            // allows for repetitive use of code, similar to for loops. but usually used for situations where the content to iterate through is indefinite
        /*int x = 0;
        while(x <10){
            System.out.println("loop number: " + x);
            x++;
        }*/

        //Do-While Loop
        /*int y = 10;
        do{
            System.out.println("Countdown: " + y);
            y--;
        }while(y > 5);*/

        // For Loop
        /*for(int i = 0; i < 10; i++){
            System.out.println("Current count: " + i);
        }*/
        /*int[] intArr = {100,200,300,400,500};
        for(int i = 0; i < intArr.length; i++){
            System.out.println(intArr[i]);
        }*/

        //For-each loop with Array Class
        /*String[] nameArray = {"John","Paul","George","Ringo"};
        for(String name : nameArray){
            System.out.println(name);
        }*/

        String[][] classroom = new String[3][3];
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";
        /*System.out.println(classroom.length);
        System.out.println(classroom[0].length);
        System.out.println(classroom[1].length);
        System.out.println(classroom[2].length);*/

        // for loop // outer loop
        /*for(int row = 0; row < 3; row++){
            // inner loop
            for(int col = 0; col < 3; col++){
                System.out.println("Classroom[" + row + "][" + col + "] = " + classroom[row][col]);
            }
        }*/

        // For-each loop with multidimensional array
        for(String[] row: classroom){
            for(String column : row){
                System.out.println(column);
            }
        }

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);
        /*System.out.println(numbers);
        numbers.forEach(num -> System.out.println("ArrayList: " + num));*/

        // for-each with hashmaps
        HashMap<String, Integer> grades = new HashMap<String, Integer>(){
            {
                put("English", 90);
                put("Math", 95);
                put("Science", 97);
                put("History", 94);
            }
        };
        // grades.forEach((subject,grade) -> System.out.print(subject + " : " + grade + "\n"));
    }
}
