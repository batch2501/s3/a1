package com.zuitt.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class FactorialNumber {
    public static void main(String[] args){
        System.out.println("Enter and integer: ");
        Scanner in = new Scanner(System.in);
        try{
            int num = in.nextInt();
            if(num < 0){
                System.out.println("Invalid input. Factorial of negative numbers is not defined.");
            }else if(num==0){
                System.out.println("The factorial of 0 is 1.");
            }else{
                int answer = 1;
                for (int i = 1; i <= num; i++){
                    answer = answer * i;
                }
                System.out.println("The factorial of " + num + " is " + answer + ".");
            }
        }catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter a valid integer");
        }
    }
}
